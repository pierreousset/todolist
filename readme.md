## Todo List Laravel4 Php

[![Build Status](https://www.youtube.com/watch?v=vTIIMJ9tUc8)](https://www.youtube.com/watch?v=vTIIMJ9tUc8)
[![GitHub release](https://img.shields.io/github/release/qubyte/rubidium.svg)](https://gitlab.com/pierreousset/todolist/tree/master)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Est une application de rappel et de note disponible partout

## Official Documentation

Pas de documantation officiel

### License

The Todolist is open-sourced software licensed under the [Ousset Pierre]()
